import Vue from 'vue'
import VueRouter from 'vue-router'
import SudokuLibrary from '../views/SudokuLibrary.vue'
import SudokuPuzzle from '../views/SudokuPuzzle.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'SudokuLibrary',
    component: SudokuLibrary
  },
  {
    path: '/puzzle/:id',
    name: 'Puzzle',
    component: SudokuPuzzle
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
